import 'package:office_x/Screens/EditEmployees.dart';
import 'package:office_x/utils/app_colors.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:flushbar/flushbar.dart';

class EmployeeDetails extends StatefulWidget {
final details;
final String tokens;

EmployeeDetails({
  this.details,
  this.tokens
});

  @override
  _EmployeeDetailsState createState() => _EmployeeDetailsState();
}

class _EmployeeDetailsState extends State<EmployeeDetails> {


 
  @override
  Widget build(BuildContext context) {

     _deleteEmployee(empid)async{
try{

     String url ="https://camera.datavivservers.in/camera/employee/$empid";
      var response = await http.delete("$url",headers:{"Authorization":"token ${widget.tokens}"});
   if(response.statusCode==204){
  
    Navigator.pop(context,true); 
 Flushbar(
          message: "Data Deleted Sucessfully",
          duration: Duration(seconds: 3),
          flushbarPosition: FlushbarPosition.BOTTOM,
      
        )..show(context);
        
        
        
   }else{
      Flushbar(
          message: "Data not Deleted Sucessfully",
          duration: Duration(seconds: 3),
          flushbarPosition: FlushbarPosition.BOTTOM,
      // flushbarStyle: FlushbarStyle.FLOATING,
        )..show(context);
     
   }
   
}catch(error){
   Flushbar(
          message:error.toString(),
          duration: Duration(seconds: 3),
          flushbarPosition: FlushbarPosition.BOTTOM,
      // flushbarStyle: FlushbarStyle.FLOATING,
        )..show(context);
   
   
}

  }
    return Scaffold(
      appBar: AppBar(
        elevation: 0.2,
        backgroundColor: AppColors.colorWhite,
        title: Text("Employee Details",style: TextStyle(color:Colors.black),),
        iconTheme: IconThemeData(
          color: Colors.black
          
        ),
      ),
          body: Padding(
        padding: EdgeInsets.only(top:90,left:18.0,right: 18.0,bottom: 18.0),
        child: Stack(
          children: <Widget>[
            Card(
              shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20.0)
                    ),
              elevation: 0.5,
              child: Container(
                // height: MediaQuery.of(context).size.height,
                child: Column(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(right:30.0,top: 30.0),
                      child: Align(
                        alignment: Alignment.topRight,
                        child: Text(widget.details['name'].toString(),style: TextStyle(fontWeight: FontWeight.bold),),
                      ),
                    ),
                    Padding(padding: EdgeInsets.only(top: 25),
                    child: Divider(thickness: 0.5,),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left:15.0,right: 15.0,top: 8.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text("Name",style: TextStyle(fontWeight: FontWeight.bold),),
                          Text(widget.details["name"].toString())
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top:5.0),
                      child: Divider(),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left:15.0,right: 15.0,top: 8.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text("Age",style: TextStyle(fontWeight: FontWeight.bold),),
                          Text(widget.details["age"].toString())
                        ],
                      ),
                    ),
                     Padding(
                      padding: const EdgeInsets.only(top:5.0),
                      child: Divider(),
                    ),
                     Padding(
                      padding: const EdgeInsets.only(left:15.0,right: 15.0,top: 8.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text("Gender",style: TextStyle(fontWeight: FontWeight.bold),),
                          Text(widget.details["gender"])
                        ],
                      ),
                    ),
                     Padding(
                      padding: const EdgeInsets.only(top:5.0),
                      child: Divider(),
                    ),
          
                    Padding(
                      padding: const EdgeInsets.only(left:15.0,right: 15.0,top: 8.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text("Email Id",style: TextStyle(fontWeight: FontWeight.bold),),
                          Text(widget.details["email"])
                        ],
                      ),
                    ),
                     Padding(
                      padding: const EdgeInsets.only(top:5.0),
                      child: Divider(),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left:15.0,right: 15.0,top: 8.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text("Phone Number",style: TextStyle(fontWeight: FontWeight.bold),),
                          Text(widget.details['contact'].toString())
                        ],
                      ),
                    ),
                     Padding(
                      padding: const EdgeInsets.only(top:5.0),
                      child: Divider(),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left:15.0,right: 15.0,top: 8.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text("Address",style: TextStyle(fontWeight: FontWeight.bold),),
                          Text(widget.details["address"])
                        ],
                      ),
                    ),
                     Padding(
                      padding: const EdgeInsets.only(top:5.0),
                      child: Divider(),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 8.0,left: 18),
                      child: Align(
                        alignment: Alignment.centerLeft,
                        child: Text("Actions",style: TextStyle(fontWeight: FontWeight.bold,color: AppColors.colorGrey,fontSize: 12.0),)),
                    ),
                     Padding(
                      padding: const EdgeInsets.only(top:5.0),
                      child: Divider(),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 8.0,left: 18),
                      child: Align(
                        alignment: Alignment.centerLeft,
                        child: InkWell(
                          onTap: (){
                            Navigator.push(context, MaterialPageRoute(
                              builder: (context)=>EditEmployees(details: widget.details,tokens: widget.tokens,) ));    

                          },
                          child: Text("Edit",style: TextStyle(color: AppColors.liteGreen,fontWeight: FontWeight.bold),))),
                    ),
                     Padding(
                      padding: const EdgeInsets.only(top:5.0),
                      child: Divider(),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 8.0,left: 18),
                      child: Align(
                        alignment: Alignment.centerLeft,
                        child: InkWell(
                          onTap: (){
                            _deleteEmployee(widget.details["id"]);
                          },
                          child: Text("Delete",style: TextStyle(color: AppColors.colorRed,fontWeight: FontWeight.bold),))),
                    ),
                     Padding(
                      padding: const EdgeInsets.only(top:5.0),
                      child: Divider(),
                    ),
                  ],
                ),
              ),
            ),
            Transform.translate(
              offset: Offset(40, -40),
                        child: CircleAvatar(
                          // backgroundImage: NetworkImage(widget.details["employee_media"].toString()),
                                  
                radius: 50.0,
              ),
            )
          ],
        ),
        
      ),
    );
  }
}
