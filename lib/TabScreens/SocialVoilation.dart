import 'package:office_x/utils/BaseUrl.dart';
import 'package:office_x/utils/app_colors.dart';
import 'package:office_x/utils/methods.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:intl/intl.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'dart:io';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';

class SocialVoilation extends StatefulWidget {
  final token;

  SocialVoilation({this.token});

  @override
  _SocialVoilationState createState() => _SocialVoilationState();
}

class _SocialVoilationState extends State<SocialVoilation> {
  String selectedDate;
  getSocialVoilationDetails() async {
    String url = BaseUrl.url + "socialdistance/?dt=$selectedDate&store_id=9";

    try {
      // Map headers ={"Authorization":"token ${widget.token}"};
      var response = await http
          .get("$url", headers: {"Authorization": "token ${widget.token}"});
      // print(response.bodyBytes);
      print(response.statusCode);
      var jsonObject = json.decode(response.body);

      if (response.statusCode == 200) {
        return jsonObject;
      } else if (response.statusCode == 204) {
        print("test" + response.body);
        Methods.showSnackBar("Internal server Error", context);
        return null;
      } else {
        print(response.body);
        Methods.showSnackBar("Internal server Error", context);
        return null;
      }
    } on SocketException catch (error) {
      Methods.showSnackBar(error.toString(), context);
    } catch (e) {
      print(e);
      //  Methods.showSnackBar(e.toString(), context);
    }
  }

  pickDate() async {
    DatePicker.showDatePicker(
      context,
      showTitleActions: true,
      minTime: DateTime(2019, 1, 1),
      maxTime: DateTime(2100, 1, 1),
      theme: DatePickerTheme(
          headerColor: Colors.orange,
          backgroundColor: Colors.blue,
          itemStyle: TextStyle(
              color: Colors.white, fontWeight: FontWeight.bold, fontSize: 18),
          doneStyle: TextStyle(color: Colors.white, fontSize: 16)),
      onChanged: (date) {
        print('change $date in time zone ' +
            date.timeZoneOffset.inHours.toString());
      },
      onConfirm: (date) {
        var dates = DateTime.parse(date.toString());
        var formattedDate = "${dates.year}-${dates.month}-${dates.day}";
        print('$formattedDate');

        setState(() {
          selectedDate = formattedDate;
        });
      },
    );
  }

  currentDate() {
    final DateTime now = DateTime.now();
    final String formatter = DateFormat('yyyy-MM-dd').format(now);
    // final String formatted = DateFormat.format(now);
    print("Date" + formatter);
    setState(() {
      selectedDate = formatter;
    });
  }

  @override
  void initState() {
    super.initState();
    currentDate();
    getSocialVoilationDetails();
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    /*24 is for notification bar on Android*/
    final double itemHeight = (size.height - kToolbarHeight - 24) / 2.6;
    final double itemWidth = size.width / 2;
    return Scaffold(
      body: FutureBuilder(
        future: getSocialVoilationDetails(),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            if (snapshot.data == null) {
              return Center(
                  child: Image(image: AssetImage("assets/noData.png")));
            } else {
              return GridView.builder(
                  shrinkWrap: true,
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 2,
                      childAspectRatio: itemWidth / itemHeight),
                  itemCount: snapshot.data.length,
                  itemBuilder: (context, index) {
                    //  selectedItemValue.add("NONE");

                    return Container(
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          border: Border.all(
                            color: Color(0xffc4c4c4),
                          )),
                      padding: EdgeInsets.all(8),
                      margin: EdgeInsets.all(8),
                      child: Column(
                        children: [
                          //Product Image
                          Container(
                            height: MediaQuery.of(context).size.height / 4,
                            width: MediaQuery.of(context).size.width / 2,
                            child: Image(
                              fit: BoxFit.fill,
                              // image: NetworkImage(
                              //     "https://images3.alphacoders.com/196/thumb-1920-196149.jpg"),
                              image:
                                  NetworkImage(snapshot.data[index]["img_url"]),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 18.0),
                            child: Text("Social Distance"),
                          )
                        ],
                      ),
                    );
                  });
            }
          } else if (snapshot.hasError) {
            return Center(
              child: Text("Internal Server Error"),
            );
          } else {
            return Center(
                child: SpinKitWave(
              color: AppColors.colorBlue,
            ));
          }
        },
      ),
      floatingActionButton: FloatingActionButton(
          heroTag: "FAB1",
          backgroundColor: AppColors.colorBlue,
          child: Icon(Icons.calendar_today),
          onPressed: () {
            pickDate();
          }),
    );
  }
}
