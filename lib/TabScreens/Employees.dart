import 'package:office_x/Screens/AddEmployees.dart';
import 'package:office_x/Screens/EmployeeDetails.dart';
import 'package:office_x/utils/BaseUrl.dart';
import 'package:office_x/utils/app_colors.dart';
import 'package:office_x/utils/methods.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class EmployeesPage extends StatefulWidget {
  final String token;

  EmployeesPage({this.token});

  @override
  _EmployeesPageState createState() => _EmployeesPageState();
}

class _EmployeesPageState extends State<EmployeesPage> {
  _navigateToAddEmployees() {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => AddEmployees(
                  tokens: widget.token,
                ))).then((value) {
      setState(() {});
    });
  }

  Future getEmployees() async {
    String url1 = BaseUrl.url + 'employee/?store_id=9';

    try {
      var response = await http
          .get("$url1", headers: {"Authorization": "token ${widget.token}"});
      var jsonObject = json.decode(response.body);

      return jsonObject;
    } catch (error) {
      Methods.showSnackBar(error.toString(), context);
    }
  }

  @override
  Widget build(BuildContext context) {
    _navigateTODetails(detail) {
      Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) =>
                      EmployeeDetails(details: detail, tokens: widget.token)))
          .then((value) {
        setState(() {});
      });
    }

    return Scaffold(
      body: Padding(
        padding:
            EdgeInsets.only(left: 20.0, right: 20.0, bottom: 20.0, top: 30.0),
        child: Stack(
          children: <Widget>[
            Card(
              elevation: 0.5,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20.0)),
              child: Container(
                height: MediaQuery.of(context).size.height / 1.3,
                width: MediaQuery.of(context).size.width,
                child: Padding(
                  padding: const EdgeInsets.only(top: 10.0),
                  child: FutureBuilder(
                    future: getEmployees(),
                    builder: (context, snapshot) {
                      if (snapshot.connectionState == ConnectionState.done) {
                        if (snapshot.data.length == 0) {
                          return Center(
                            child: Text("Add Your Employees here"),
                          );
                        } else {
                          return ListView.builder(
                            itemCount: snapshot.data.length,
                            itemBuilder: (context, index) {
                              return Padding(
                                padding: const EdgeInsets.only(top: 8.0),
                                child: InkWell(
                                  onTap: () {
                                    _navigateTODetails(snapshot.data[index]);
                                  },
                                  child: ListTile(
                                    //  title: Text(snapshot.data[index]["name"].toString()),
                                    leading: CircleAvatar(
                                        child: snapshot.data[index]["gender"] ==
                                                "M"
                                            ? Text(
                                                "M",
                                                style: TextStyle(fontSize: 25),
                                              )
                                            : Text("F",
                                                style:
                                                    TextStyle(fontSize: 25))),

                                    trailing: Icon(Icons.arrow_right),
                                    title: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: <Widget>[
                                        Flexible(
                                            child: Text(
                                          snapshot.data[index]["name"],
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              fontSize: 15),
                                        )),
                                        Column(
                                          children: <Widget>[
                                            Text("Entry:1:30 PM",
                                                style: TextStyle(fontSize: 10)),
                                            Text("Exit:1:40 PM",
                                                style: TextStyle(fontSize: 10))
                                          ],
                                        )
                                      ],
                                    ),
                                  ),
                                ),
                              );
                            },
                          );
                        }
                      } else if (snapshot.hasError) {
                        return Center(
                          child: Text("Internal Server Error"),
                        );
                      } else {
                        return Center(
                          child: SpinKitWave(color: AppColors.colorBlue),
                        );
                      }
                    },
                  ),
                ),
              ),
            ),
            Transform.translate(
              offset: Offset(40, -10),
              child: Container(
                height: MediaQuery.of(context).size.height / 27,
                width: MediaQuery.of(context).size.width / 3.1,
                child: Center(
                  child: Text("Employees"),
                ),
                decoration: BoxDecoration(
                    color: Colors.grey[400],
                    borderRadius: BorderRadius.circular(20.0)),
              ),
            )
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        child: Center(
            child: Image.asset(
          "assets/add.ico",
          width: 25,
          color: AppColors.colorWhite,
        )),
        backgroundColor: AppColors.colorBlue,
        elevation: 2.0,
        onPressed: () {
          _navigateToAddEmployees();
        },
      ),
    );
  }
}
