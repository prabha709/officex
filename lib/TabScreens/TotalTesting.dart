import 'package:office_x/barChartData/Sample_view.dart';
import 'package:office_x/utils/BaseUrl.dart';
import 'package:syncfusion_flutter_charts/charts.dart';
import 'package:intl/intl.dart';
import 'dart:io';
import 'package:office_x/utils/app_colors.dart';
import 'package:office_x/utils/methods.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:pie_chart/pie_chart.dart';
import 'package:animated_floatactionbuttons/animated_floatactionbuttons.dart';

class TestingAnalysisPage extends StatelessWidget {
  final token;

  TestingAnalysisPage({this.token});

  @override
  Widget build(BuildContext context) {
    return Material(
      child: TotalTesting(
        token: token,
      ),
    );
  }
}

class TotalTesting extends StatefulWidget {
  final token;

  TotalTesting({this.token});

  @override
  _TotalTestingState createState() => _TotalTestingState();
}

class _TotalTestingState extends State<TotalTesting> {
  bool _dailywise = false;

  currentDate() {
    final DateTime now = DateTime.now();

    final DateFormat dateFormat = DateFormat('yyyy-MM-dd');
    dynamic currentTime = DateFormat.H().format(DateTime.now());
    final String formatted = dateFormat.format(now);

    print(currentTime);
    setState(() {
      selectedDate = formatted;
      selectedTime = currentTime;
    });
  }

  _getDefaultColumnSeries(data) {
    var teenage = (data["age_teenge"] /
            (data["age_child"] +
                data["age_teenge"] +
                data["age_adult"] +
                data["age_old"])) *
        100;
    var child = (data["age_child"] /
            (data["age_child"] +
                data["age_teenge"] +
                data["age_adult"] +
                data["age_old"])) *
        100;
    var adult = (data["age_adult"] /
            (data["age_child"] +
                data["age_teenge"] +
                data["age_adult"] +
                data["age_old"])) *
        100;
    var old = (data["age_old"] /
            (data["age_child"] +
                data["age_teenge"] +
                data["age_adult"] +
                data["age_old"])) *
        100;

    final List<ChartSampleData> chartData = <ChartSampleData>[
      ChartSampleData(x: 'Child', y: child.round()),
      ChartSampleData(x: 'Teenage', y: teenage.round()),
      ChartSampleData(x: 'Adult', y: adult.round()),
      ChartSampleData(x: 'aged', y: old.round()),
    ];
    return <ColumnSeries<ChartSampleData, String>>[
      ColumnSeries<ChartSampleData, String>(
        dataSource: chartData,
        xValueMapper: (ChartSampleData sales, _) => sales.x,
        yValueMapper: (ChartSampleData sales, _) => sales.y,
        dataLabelSettings: DataLabelSettings(
            isVisible: true, textStyle: const TextStyle(fontSize: 10)),
      )
    ];
  }

  getCount() async {
    try {
      String uri =
          BaseUrl.url + "getmanageranalysisdata/?dt=$selectedDate&store_id=5";

      var response = await http
          .get('$uri', headers: {"Authorization": "token ${widget.token}"});

      if (response.statusCode == 200) {
        var jsonObject = json.decode(response.body);
        print(jsonObject);
        return jsonObject;
      } else if (response.statusCode == 204) {
        print("Error" + response.body);
        Methods.showSnackBar("No Data Found", context);
        return null;
      } else {
        Methods.showSnackBar("No Data Found", context);
        return null;
      }
    } on SocketException catch (error) {
      Methods.showSnackBar(error.toString(), context);
    } catch (e) {
      Methods.showSnackBar(e.toString(), context);
      print(e);
    }
  }

  String selectedDate;
  String selectedTime;
  dailyData() {
    DatePicker.showDatePicker(
      context,
      showTitleActions: true,
      minTime: DateTime(2019, 1, 1),
      maxTime: DateTime(2021, 12, 31),
      onChanged: (date) {
        // print('change $date');
      },
      onConfirm: (date) {
        var dates = DateTime.parse(date.toString());
        var formattedDate = "${dates.year}-${dates.month}-${dates.day}";
        print(formattedDate);
        setState(() {
          selectedDate = formattedDate;
          _dailywise = true;
        });
        // print('confirm $date');
      },
    );
  }

  pickDate() async {
    DatePicker.showDateTimePicker(
      context,
      showTitleActions: true,
      minTime: DateTime(2019, 3, 5, 00),
      maxTime: DateTime(2021, 6, 7, 00),
      theme: DatePickerTheme(
          headerColor: Colors.orange,
          backgroundColor: Colors.blue,
          itemStyle: TextStyle(
              color: Colors.white, fontWeight: FontWeight.bold, fontSize: 18),
          doneStyle: TextStyle(color: Colors.white, fontSize: 16)),
      onChanged: (date) {
        print('change $date in time zone ' +
            date.timeZoneOffset.inHours.toString());
      },
      onConfirm: (date) {
        var dates = DateTime.parse(date.toString());
        var formattedDate = "${dates.year}-${dates.month}-${dates.day}";
        var formattedTime = "${dates.hour}";

        print('$formattedDate');
        setState(() {
          selectedDate = formattedDate;
          selectedTime = formattedTime;
          _dailywise = false;
        });
        // pickTime();
      },
    );
  }

  List<Color> colorList = [
    Color(0xff00C2FF),
    Color(0xff2C8AA7),
  ];
  @override
  void initState() {
    super.initState();
    currentDate();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: selectedTime == null
            ? Center(
                child: Text("Kindly Select Date and Time to show data"),
              )
            : Container(
                child: FutureBuilder(
                    future: getCount(),
                    builder: (context, snapshot) {
                      if (snapshot.connectionState == ConnectionState.done) {
                        if (snapshot.data == null ||
                            snapshot.data["flag"] == false) {
                          return Center(
                              child: Image(
                                  image: AssetImage("assets/noData.png")));
                        } else {
                          var malePercent = (snapshot.data["avg_male_count"] /
                                  (snapshot.data["avg_male_count"] +
                                      snapshot.data["avg_female_count"])) *
                              100;
                          var femalePercent =
                              (snapshot.data["avg_female_count"] /
                                      (snapshot.data["avg_female_count"] +
                                          snapshot.data["avg_male_count"])) *
                                  100;

                          Map<String, double> pie = {
                            'Female': femalePercent,
                            "Male": malePercent
                          };
                          return SingleChildScrollView(
                            child: Column(
                              children: [
                                Padding(
                                  padding: const EdgeInsets.all(18.0),
                                  child: Stack(
                                    children: <Widget>[
                                      Padding(
                                        padding: const EdgeInsets.all(18.0),
                                        child: Card(
                                          shape: RoundedRectangleBorder(
                                              borderRadius:
                                                  BorderRadius.circular(20.0)),
                                          elevation: 1.0,
                                          child: Container(
                                              height: MediaQuery.of(context)
                                                      .size
                                                      .height /
                                                  3,
                                              width: MediaQuery.of(context)
                                                  .size
                                                  .width,
                                              child: Center(
                                                  child: Padding(
                                                      padding:
                                                          const EdgeInsets.all(
                                                              8.0),
                                                      child: PieChart(
                                                          dataMap: pie,
                                                          colorList: colorList,
                                                          chartValuesOptions:
                                                              ChartValuesOptions(
                                                            showChartValuesInPercentage:
                                                                true,
                                                          ))
                                                      // child: Image.network(snapshot.data["store_heat_map"]),
                                                      ))),
                                        ),
                                      ),
                                      Transform.translate(
                                        offset: Offset(40, 10),
                                        child: Container(
                                          height: MediaQuery.of(context)
                                                  .size
                                                  .height /
                                              27,
                                          width: MediaQuery.of(context)
                                                  .size
                                                  .width /
                                              2.8,
                                          child: Center(
                                            child: Text("Gender"),
                                          ),
                                          decoration: BoxDecoration(
                                              color: Colors.grey[400],
                                              borderRadius:
                                                  BorderRadius.circular(20.0)),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.all(18.0),
                                  // child: Stack(
                                  //   children: <Widget>[
                                  child: Padding(
                                      padding: const EdgeInsets.all(18.0),
                                      child: Card(
                                          shape: RoundedRectangleBorder(
                                              borderRadius:
                                                  BorderRadius.circular(20.0)),
                                          elevation: 1.0,
                                          child: Container(
                                              height: MediaQuery.of(context)
                                                      .size
                                                      .height /
                                                  3,
                                              width: MediaQuery.of(context)
                                                  .size
                                                  .width,
                                              child: Center(
                                                child: SfCartesianChart(
                                                  // backgroundColor:Color(0xff00C2FF) ,
                                                  // borderColor: Color(0xff00C2FF),
                                                  plotAreaBorderWidth: 0,
                                                  title:
                                                      ChartTitle(text: "Age"),
                                                  primaryXAxis: CategoryAxis(
                                                    majorGridLines:
                                                        MajorGridLines(
                                                            width: 0),
                                                  ),
                                                  primaryYAxis: NumericAxis(
                                                      axisLine:
                                                          AxisLine(width: 0),
                                                      labelFormat: '{value}%',
                                                      majorTickLines:
                                                          MajorTickLines(
                                                              size: 0)),
                                                  tooltipBehavior:
                                                      TooltipBehavior(
                                                          enable: true,
                                                          header: '',
                                                          canShowMarker: false),
                                                  series:
                                                      _getDefaultColumnSeries(
                                                          snapshot.data),
                                                ),
                                              )

                                              // child: Image.network(snapshot.data["store_heat_map"]),
                                              ))),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(
                                      left: 18.0, right: 18.0),
                                  child: Card(
                                    // color: Colors.green,
                                    elevation: 1.0,
                                    shape: RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(20.0)),
                                    child: Container(
                                      height:
                                          MediaQuery.of(context).size.height /
                                              9,
                                      child: Center(
                                        child: ListTile(
                                          title: Text(
                                            "Number of Customers",
                                            style: TextStyle(
                                                fontWeight: FontWeight.bold),
                                          ),
                                          trailing: Padding(
                                            padding: const EdgeInsets.only(
                                                right: 30.0),
                                            child: Container(
                                              width: 80.0,
                                              height: 80.0,
                                              child: Center(
                                                child: Text(
                                                  snapshot
                                                      .data["customer_walkin"]
                                                      .toString(),
                                                  // style: TextStyle(fontSize: 20),
                                                ),
                                              ),
                                              decoration: BoxDecoration(
                                                  shape: BoxShape.circle,
                                                  color: AppColors.colorGrey),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(
                                      left: 18.0, right: 18.0),
                                  child: Card(
                                    // color: Colors.green,
                                    elevation: 1.0,
                                    shape: RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(20.0)),
                                    child: Container(
                                      height:
                                          MediaQuery.of(context).size.height /
                                              9,
                                      child: Center(
                                        child: ListTile(
                                          title: Text(
                                            "Total Walkins",
                                            style: TextStyle(
                                                fontWeight: FontWeight.bold),
                                          ),
                                          trailing: Padding(
                                            padding: const EdgeInsets.only(
                                                right: 30.0),
                                            child: Container(
                                              width: 80.0,
                                              height: 80.0,
                                              child: Center(
                                                child: Text(
                                                  snapshot.data["total_in"]
                                                      .toString(),
                                                  // style: TextStyle(fontSize: 20),
                                                ),
                                              ),
                                              decoration: BoxDecoration(
                                                  shape: BoxShape.circle,
                                                  color: AppColors.colorGrey),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(
                                      left: 18.0, right: 18.0),
                                  child: Card(
                                    // color: Colors.green,
                                    elevation: 1.0,
                                    shape: RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(20.0)),
                                    child: Container(
                                      height:
                                          MediaQuery.of(context).size.height /
                                              9,
                                      child: Center(
                                        child: ListTile(
                                          title: Text(
                                            "Total WalkOuts",
                                            style: TextStyle(
                                                fontWeight: FontWeight.bold),
                                          ),
                                          trailing: Padding(
                                            padding: const EdgeInsets.only(
                                                right: 30.0),
                                            child: Container(
                                              width: 80.0,
                                              height: 80.0,
                                              child: Center(
                                                child: Text(
                                                  snapshot.data["total_out"]
                                                      .toString(),
                                                  // style: TextStyle(fontSize: 20),
                                                ),
                                              ),
                                              decoration: BoxDecoration(
                                                  shape: BoxShape.circle,
                                                  color: AppColors.colorGrey),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          );
                        }
                      } else if (snapshot.hasError) {
                        return Center(
                          child: Text("Internal Server Error"),
                        );
                      } else {
                        return Center(
                          child: SpinKitWave(
                            color: AppColors.colorBlue,
                          ),
                        );
                      }
                    })),
        floatingActionButton: AnimatedFloatingActionButton(
            fabButtons: <Widget>[
              FloatingActionButton(
                  heroTag: "fab1",
                  backgroundColor: AppColors.colorBlue,
                  child: Icon(Icons.date_range),
                  onPressed: () {
                    dailyData();
                  }),
              FloatingActionButton(
                  heroTag: "fab2",
                  backgroundColor: AppColors.colorBlue,
                  child: Icon(Icons.timer),
                  onPressed: () {
                    pickDate();
                  }),
            ],
            colorStartAnimation: AppColors.colorBlue,
            colorEndAnimation: AppColors.colorRed,
            animatedIconData: AnimatedIcons.menu_close));
  }
}
