import 'dart:io';
import 'package:office_x/utils/BaseUrl.dart';
import 'package:office_x/utils/app_colors.dart';
import 'package:office_x/utils/methods.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:intl/intl.dart';
import 'package:animated_floatactionbuttons/animated_floatactionbuttons.dart';

class AnalyticsModelData extends StatelessWidget {
  final String token;

  AnalyticsModelData({this.token});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: AnalyticsPage(
        token: token,
      ),
    );
  }
}

class AnalyticsPage extends StatefulWidget {
  final token;

  AnalyticsPage({this.token});

  @override
  _AnalyticsPageState createState() => _AnalyticsPageState();
}

class _AnalyticsPageState extends State<AnalyticsPage> {
  String selectedDate;
  bool filterClass = false;
  bool selectedFilter = false;
  int listcount;

  String updatedclassType;
  currentDate() {
    final DateTime now = DateTime.now();
    final String formatter = DateFormat('yyyy-MM-dd').format(now);
    // final String formatted = DateFormat.format(now);
    print("Date" + formatter);
    setState(() {
      selectedDate = formatter;
    });
  }

  @override
  void initState() {
    super.initState();
    currentDate();
  }

  getModeAnalysis() async {
    String url = BaseUrl.url + "modelanalysis/?dt=$selectedDate&store_id=9";
    String url1 = BaseUrl.url +
        "modelanalysis/?dt=$selectedDate&cltype=$updatedclassType";
    try {
      // Map headers ={"Authorization":"token ${widget.token}"};
      var response = await http.get(filterClass == false ? "$url" : "$url1",
          headers: {"Authorization": "token ${widget.token}"});
      // print(response.bodyBytes);
      // print(response.statusCode);

      if (response.statusCode == 200) {
        var jsonObject = json.decode(response.body);
        return jsonObject;
      } else if (response.statusCode == 204) {
        print("test" + response.body);
        Methods.showSnackBar("No Record Found", context);
        return null;
      } else {
        print(response.body);
        Methods.showSnackBar("Internal server Error", context);
        return null;
      }
    } on SocketException catch (error) {
      Methods.showSnackBar(error.toString(), context);
    } catch (e) {
      print(e);
      //  Methods.showSnackBar(e.toString(), context);
    }
  }

  pickDate() async {
    DatePicker.showDatePicker(
      context,
      showTitleActions: true,
      minTime: DateTime(2019, 1, 1),
      maxTime: DateTime(2100, 1, 1),
      theme: DatePickerTheme(
          headerColor: Colors.orange,
          backgroundColor: Colors.blue,
          itemStyle: TextStyle(
              color: Colors.white, fontWeight: FontWeight.bold, fontSize: 18),
          doneStyle: TextStyle(color: Colors.white, fontSize: 16)),
      onChanged: (date) {
        print('change $date in time zone ' +
            date.timeZoneOffset.inHours.toString());
      },
      onConfirm: (date) {
        var dates = DateTime.parse(date.toString());
        var formattedDate = "${dates.year}-${dates.month}-${dates.day}";
        print('$formattedDate');
        if (selectedFilter == true) {
          _settingModalBottomSheet(context);
          setState(() {
            selectedFilter = false;
            selectedDate = formattedDate;
          });
        } else {
          setState(() {
            selectedDate = formattedDate;
            filterClass = false;
          });
        }
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    /*24 is for notification bar on Android*/
    final double itemHeight = (size.height - kToolbarHeight - 24) / 1.95;
    final double itemWidth = size.width / 2;
    return Scaffold(
        body: selectedDate == null
            ? Center(child: Text("Please Select the Date to show Data"))
            : FutureBuilder(
                future: getModeAnalysis(),
                builder: (context, snapshot) {
                  if (snapshot.connectionState == ConnectionState.done) {
                    if (snapshot.data == null) {
                      return Center(
                          child: Image(image: AssetImage("assets/noData.png")));
                    } else {
                      return Stack(
                        children: [
                          GridView.builder(
                              shrinkWrap: true,
                              gridDelegate:
                                  SliverGridDelegateWithFixedCrossAxisCount(
                                      crossAxisCount: 2,
                                      childAspectRatio: itemWidth / itemHeight),
                              itemCount: snapshot.data.length,
                              itemBuilder: (context, index) {
                                //  selectedItemValue.add("NONE");

                                return DropDownItems(
                                    indexData: snapshot.data[index],
                                    tokens: widget.token);
                              }),
                          Positioned(
                            top: 0,
                            right: 0,
                            child: FloatingActionButton(
                                heroTag: "FAB3",
                                backgroundColor: AppColors.colorWhite,
                                elevation: 5.0,
                                child: Text(
                                  snapshot.data.length.toString(),
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      color: Colors.black),
                                ),
                                onPressed: () {
                                  // pickDate();
                                }),
                          ),
                        ],
                      );
                    }
                  } else if (snapshot.hasError) {
                    return Center(
                      child: Text("Internal Server Error"),
                    );
                  } else {
                    return Center(
                        child: SpinKitWave(
                      color: AppColors.colorBlue,
                    ));
                  }
                },
              ),
        floatingActionButton: AnimatedFloatingActionButton(
            fabButtons: <Widget>[
              FloatingActionButton(
                  heroTag: "FAB1",
                  backgroundColor: AppColors.colorBlue,
                  child: Icon(Icons.calendar_today),
                  onPressed: () {
                    pickDate();
                  }),
              FloatingActionButton(
                  heroTag: "FAB2",
                  backgroundColor: AppColors.colorBlue,
                  child: Icon(Icons.filter_list),
                  onPressed: () {
                    selectedFilter = true;

                    pickDate();
                  }),
            ],
            colorStartAnimation: AppColors.colorBlue,
            colorEndAnimation: AppColors.colorRed,
            animatedIconData: AnimatedIcons.menu_close));
  }

  void _settingModalBottomSheet(context) {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext bc) {
          return Container(
            child: new Wrap(
              children: <Widget>[
                new ListTile(
                    // leading: new Icon(Icons.music_note),
                    title: new Text('Employee'),
                    onTap: () {
                      setState(() {
                        updatedclassType = "E";
                        filterClass = true;
                      });

                      Navigator.pop(context);
                    }),
                new ListTile(
                  // leading: new Icon(Icons.videocam),
                  title: new Text('Customer'),
                  onTap: () {
                    setState(() {
                      updatedclassType = "C";
                      filterClass = true;
                    });
                    Navigator.pop(context);
                  },
                ),
                new ListTile(
                  // leading: new Icon(Icons.videocam),
                  title: new Text('Others'),
                  onTap: () {
                    setState(() {
                      updatedclassType = "O";
                      filterClass = true;
                    });
                    Navigator.pop(context);
                  },
                ),
                new ListTile(
                  // leading: new Icon(Icons.videocam),
                  title: new Text('Financiers'),
                  onTap: () {
                    setState(() {
                      updatedclassType = "F";
                      filterClass = true;
                    });
                    Navigator.pop(context);
                  },
                ),
                new ListTile(
                  // leading: new Icon(Icons.videocam),
                  title: new Text('Promoters'),
                  onTap: () {
                    setState(() {
                      updatedclassType = "P";
                      filterClass = true;
                    });
                    Navigator.pop(context);
                  },
                ),
              ],
            ),
          );
        });
  }
}

class DropDownItems extends StatefulWidget {
  final indexData;
  final tokens;

  DropDownItems({this.indexData, this.tokens});
  @override
  _DropDownItemsState createState() => _DropDownItemsState();
}

class _DropDownItemsState extends State<DropDownItems> {
  String selectedItemValue;
  bool flag = false;
  String _classType;
  _updateData(indexData) async {
    String url =
        "https://camera.datavivservers.in/camera/modelanalysis/${indexData["id"]}";
    try {
      Map data = {
        // "img_url": indexData["img_url"].toString(),
        // "timestamp": indexData["timestamp"].toString(),
        // "store": indexData["store"].toString(),
        "updatedclasstype": _classType.toString(),
        "flag": true.toString()
      };
      var response = await http.patch("$url",
          body: data, headers: {"Authorization": "token ${widget.tokens}"});
      var jsonObject = json.decode(response.body);
      if (response.statusCode == 200) {
        setState(() {
          headings = selectedItemValue;
          changeColor = true;
        });
        Methods.showSnackBar("Updation Done", context);
      } else {
        return Methods.showSnackBar("Updation Not Done", context);
      }
      return jsonObject;
    } on SocketException catch (error) {
      Methods.showSnackBar(error.toString(), context);
    } catch (e) {
      print(e);
      // Methods.showSnackBar(e.toString(), context);
    }
  }

  String result() {
    if (widget.indexData["flag"] == false) {
      if (widget.indexData["classtype"] == "E") {
        return "Employee";
      } else if (widget.indexData["classtype"] == "C") {
        return "Customer";
      } else if (widget.indexData["classtype"] == "F") {
        return "Financiers";
      } else if (widget.indexData["classtype"] == "P") {
        return "Promoters";
      } else {
        return "Others";
      }
    } else {
      if (widget.indexData["updatedclasstype"] == "E") {
        return "Employee";
      } else if (widget.indexData["updatedclasstype"] == "C") {
        return "Customer";
      } else if (widget.indexData["updatedclasstype"] == "F") {
        return "Financiers";
      } else if (widget.indexData["updatedclasstype"] == "P") {
        return "Promoters";
      } else {
        return "Others";
      }
    }
  }

  bool changeColor = false;
  String headings;
  @override
  void initState() {
    super.initState();
    headings = result();
    selectedItemValue = result();
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {},
      child: Container(
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            border: Border.all(
              color: Color(0xffc4c4c4),
            )),
        padding: EdgeInsets.all(8),
        margin: EdgeInsets.all(8),
        child: Column(
          children: [
            //Product Image
            Container(
              height: MediaQuery.of(context).size.height / 4,
              width: MediaQuery.of(context).size.width / 2,
              child: Image(
                fit: BoxFit.fill,
                // image: NetworkImage(
                //     "https://images3.alphacoders.com/196/thumb-1920-196149.jpg"),
                image: NetworkImage(widget.indexData["img_url"]),
              ),
            ),

            Expanded(
              child: Column(
                children: [
                  Text(headings,
                      style: TextStyle(
                          color: widget.indexData["flag"] == true ||
                                  changeColor == true
                              ? Colors.red
                              : Colors.black)),
                  Text(widget.indexData["timestamp"].toString()),
                ],
              ),
            ),

            DropdownButton(
              //  isDense: true,
              icon: Icon(Icons.arrow_downward),
              iconSize: 24,
              elevation: 16,
              style: TextStyle(color: Colors.deepPurple),
              underline: Container(
                height: 2,
                color: Colors.deepPurpleAccent,
              ),
              onChanged: (value) {
                setState(() {
                  flag = true;
                  selectedItemValue = value;

                  _classType = selectedItemValue[0];
                });
                print(selectedItemValue);
                print(_classType);
              },
              value: selectedItemValue,
              items: _dropDownItem(),
            ),

            FlatButton(
              onPressed: flag == false
                  ? null
                  : () async {
                      var response = await _updateData(widget.indexData);
                      // print(response);
                      // setState(() {
                      //   headings = selectedItemValue;
                      // });
                    },
              disabledColor: Colors.blueGrey,
              splashColor: Colors.redAccent,
              color: Theme.of(context).accentColor,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10),
              ),
              child: Text(
                'Update',
                style: TextStyle(color: Colors.white),
              ),
            )
          ],
        ),
      ),
    );
  }
}

List<DropdownMenuItem<String>> _dropDownItem() {
  List<String> ddl = [
    "NONE",
    "Employee",
    "Customer",
    "Others",
    "Financiers",
    "Promoters"
  ];
  return ddl
      .map((value) => DropdownMenuItem(
            value: value,
            child: Text(value),
          ))
      .toList();
}
