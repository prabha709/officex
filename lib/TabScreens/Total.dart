import 'dart:io';

import 'package:office_x/utils/app_colors.dart';
import 'package:office_x/utils/methods.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'dart:convert';
import 'package:http/http.dart'as http;

class Total extends StatefulWidget {
final String token;

Total({
  this.token
});

  @override
  _TotalState createState() => _TotalState();
}

class _TotalState extends State<Total> {

Future getData() async {
    try{
      // String url1 ='http://34.66.241.156/camera/totaldisplay/';
String url1="http://34.122.179.12/camera/client/";
    
    var response = await http
        .get("$url1", headers: {"Authorization": "token ${widget.token}"});
    var jsonObject = json.decode(response.body);
    if(response.statusCode==200){
      return jsonObject[0];

    }else{
      print(response.statusCode);
    }
  
  
    }on SocketException catch(error){
      Methods.showSnackBar(error.toString(), context);
    }catch(e){
      print(e);
      // Methods.showSnackBar(e.toString(), context);
    }
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: FutureBuilder(
        future: getData(),
        builder: (context, snapshot){
          if(snapshot.connectionState ==  ConnectionState.done){
            if(snapshot.data == null){
              return Center(
                          child: Text("Customer Details not yet processed"),
                        );
            }else{
                          return SingleChildScrollView(
              child: Column(
          children: <Widget>[
            Padding(
                  padding: const EdgeInsets.only(top:18.0,left:18.0,right: 18.0),
                  child: Card(
                    // color: Colors.green,
                    elevation: 0.5,
                    shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20.0)
            ),
                    child: Container(
                      height: MediaQuery.of(context).size.height/6.5,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                         Padding(
                           padding: const EdgeInsets.only(top:18.0,left: 14.0),
                           child: Column(
                             children: <Widget>[
                               Text("Total walkings"),
                               Row(
                                 children: <Widget>[
                                   Icon(Icons.keyboard_arrow_up,color: AppColors.liteGreen,),
                                   Text("10.20%",style: TextStyle(color: AppColors.liteGreen,fontWeight: FontWeight.bold),)
                                 ],
                               )

                             ],
                           ),
                         ),
                          Padding(
                            padding: const EdgeInsets.only(right:30.0),
                            child: Container(
                              width: 60.0,
                              height: 60.0,
                              child: Center(
                                child: Text("6"),
                                // child: Text(snapshot.data[0]["total_walkings"].toString(),style: TextStyle(fontSize: 20),),
                              ),
                                 decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              color: AppColors.colorGrey),

                            ),
                          )
                        ],
                      ),

                    ),
                  ),
                ),
                Padding(
          padding: EdgeInsets.only(left: 20.0,right:20.0,bottom: 20.0,top: 30.0),
              child: Stack(
                
                children: <Widget>[


                  Card(
                    //  color: Colors.blue,
                    elevation: 0.5,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20.0)
                    ),
                    child: Container(
                       height: MediaQuery.of(context).size.height/3.5,
                      width: MediaQuery.of(context).size.width,
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: ListView.builder(
                          shrinkWrap: true,
                          itemCount: snapshot.data.length == null ?0: snapshot.data.length,
                          itemBuilder: (context, index){
                            return Column(
                              children: <Widget>[
                                Padding(
                                padding: const EdgeInsets.only(top:12.0,left: 10,right: 10),
                                child: ExpansionTile(
                                  leading: CircleAvatar(
                                  backgroundImage: NetworkImage("https://images.unsplash.com/photo-1541423408854-5df732b6f6d1?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjI0MX0&auto=format&fit=crop&w=750&q=80"),
                                    ),
                                    title: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Text(snapshot.data[index]["client_name"],style: TextStyle(fontWeight: FontWeight.bold,fontSize: 12),),
                              Column(
                               
                                children: <Widget>[
                                  Text("Entry-" + snapshot.data[index]["entry_time"].toString(),style:TextStyle(fontSize: 10)),
                                  Text("Exit-" + snapshot.data[index]["exit_time"].toString(),style:TextStyle(fontSize: 10))
                                ],
                              )
                            ],
                          ),
                          children: <Widget>[
                            Container(
                              child: Column(
                                children: <Widget>[
                                  Container(
                                    height: MediaQuery.of(context).size.height/3.5,
                                    color: Colors.green[50],
                                    child: Image.network(snapshot.data[index]["customer_flow"])
                                  ),
                                   Padding(
                        padding: const EdgeInsets.only(left:15.0,right: 15.0,top: 15.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text("Number of Family Members",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 12),),
                            Text(snapshot.data[index]["number_of_family_member"].toString(),style: TextStyle(fontSize: 12),)
                          ],
                        ),
                      ),
                       Padding(
                        padding: const EdgeInsets.only(left:15.0,right: 15.0,top: 15.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text("Client Gender",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 12),),
                            Text(snapshot.data[index]["client_gender"],style: TextStyle(fontSize: 12),)
                          ],
                        ),
                      ),
                       Padding(
                        padding: const EdgeInsets.only(left:15.0,right: 15.0,top: 15.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text("Client Age",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 12),),
                            Text(snapshot.data[index]["client_age"].toString(),style: TextStyle(fontSize: 12),)
                          ],
                        ),
                      ),
                       Padding(
                        padding: const EdgeInsets.only(left:15.0,right: 15.0,top: 15.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text("Client Purchase Amount",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 12),),
                            Text("Rs." + snapshot.data[index]["client_purchase_amount"].toString(),style: TextStyle(fontSize: 12),)
                          ],
                        ),
                      ),
                       Padding(
                      padding: const EdgeInsets.only(left:15.0,right: 15.0,top: 15.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text("Client Previous Visit Time",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 12),),
                          Flexible(child: Text(snapshot.data[index]["client_previous_visit_time"].toString(),style: TextStyle(fontSize: 12),))
                        ],
                      ),
                    ),
                     Padding(
                      padding: const EdgeInsets.only(left:15.0,right: 15.0,top: 15.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text("Client visit this month",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 12),),
                          Text(snapshot.data[index]["client_visits_this_month"].toString(),style: TextStyle(fontSize: 12),)
                        ],
                      ),
                    ),
                     Padding(
                      padding: const EdgeInsets.only(left:15.0,right: 15.0,top: 10.0,bottom: 10),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text("Edit",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 12),),
                          Icon(Icons.arrow_right)
                        ],
                      ),
                    ),

                                ],
                              ),
                            ),
                          ],
                                )
                                )
                              ],
                            );
                          },
                        ),
                      ),
                     
                    ),

                  ),
                  Transform.translate(
                    offset: Offset(40, -10),
                                  child: Container(
  
  height: MediaQuery.of(context).size.height/27,
  width: MediaQuery.of(context).size.width/3.1,
  child: Center(
        child: Text("Clients"),
  ),
  decoration: BoxDecoration(
        color: Colors.grey[400],
        borderRadius: BorderRadius.circular(20.0)
  ),
),
                  ),
                ],
              ),
        ),
          ],
        ),
      );
            }
          }else if(snapshot.hasError){
            return Center(
                          child:Text("Internal Server Error") ,
                        );
          }else{
             return Center(
                          child: SpinKitWave(color: AppColors.colorBlue),
                        );
          }

  


        },
      )

    );
  }
}