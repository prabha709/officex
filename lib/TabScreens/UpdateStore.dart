import 'dart:io';
import 'package:office_x/utils/app_colors.dart';
import 'package:office_x/utils/methods.dart';
import 'package:office_x/utils/raised_button.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart'as http;
import 'dart:convert';
import 'package:file_picker/file_picker.dart';

class UpdateStore extends StatefulWidget {
final String token;
final details;

UpdateStore({
  this.token,
  this.details

});

  @override
  _UpdateStoreState createState() => _UpdateStoreState();
}

class _UpdateStoreState extends State<UpdateStore> {
  final GlobalKey<FormState>_formkey = GlobalKey<FormState>();
bool loading = false;
   Future updateData(context) async {
 if(_formkey.currentState.validate()){
        setState(() {
          loading = true;
          
        });
        _formkey.currentState.save();
    try{
      String url1 ='https://camera.datavivservers.in/camera/store/8';

     Map data ={
       "location":_location,
       "total_camera":_totalC,
       "outer_camera_channel_no":_outerC,
       "billing_camera_channel_no":_billingC,
       

       };
    var response = await http
        .patch("$url1", 
        body: data,
        headers: {"Authorization": "token ${widget.token}"});
    var jsonObject = json.decode(response.body);
  if(response.statusCode ==200){
     setState(() {
    loading = false;
 });
 Navigator.pop(context);
    // return jsonObject;

  }else{
     setState(() {
    loading = false;
 });
    return Methods.showSnackBar("Internal Sever error", context);
  }
  
    }on SocketException catch(error){
       setState(() {
    loading = false;
 });
      Methods.showSnackBar(error.toString(), context);
    }catch(e){
       setState(() {
    loading = false;
 });
      Methods.showSnackBar(e.toString(), context);
    }
 }
  }
 
      
  

  String _storeName;
  String _email;
  String _location;
  String _activeC;
  String _totalC;
  String _outerC;
  String _billingC;
  

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        iconTheme: IconThemeData(
          color: AppColors.colorBlack
        ),
backgroundColor: AppColors.colorWhite,
        title: Text("Update Store",style: TextStyle(color: AppColors.colorBlack,)),
      ),
      body: Form(
        key: _formkey,
              child: SingleChildScrollView(
                child: Column(
              children: <Widget>[
                Padding(
          padding: const EdgeInsets.all(18.0),
          child: Card(
            // color: Colors.green,
            elevation: 0.5,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20.0)
            ),
            child: Container(
              // height: MediaQuery.of(context).size.height/3.7,
              width: MediaQuery.of(context).size.width,
              child: Column(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(left:20.0,right: 20.0,top:5.0),
                      child: TextFormField(
                        decoration: InputDecoration(
                          labelText: "Store Name"
                        ),
                      ),
                    ),
             
                    Divider(),
                     Padding(
                      padding: const EdgeInsets.only(left:20.0,right: 20.0,top:5.0),
                      child: TextFormField(
                        decoration: InputDecoration(
                          labelText: "Email Id"
                        ),
                      ),
                    ),
                    Divider(),
                    Padding(
                      padding: const EdgeInsets.only(left:20.0,right: 20.0,top:5.0),
                      child: TextFormField(
                        initialValue: widget.details["location"],
                        decoration: InputDecoration(
                          labelText: "Location",
                        ),
                        validator: (val){
          if(val.isEmpty){
            return "Please Enter the Location";
          }else{
            return null;
          }
        },
         onSaved: (val)=>_location=val,
                      ),
                    ),
                   Divider(),
                     
                  ],
                ),
              
            ),

          ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left:18.0,right: 18.0),
                  child: Card(
                    // color: Colors.green,
                    elevation: 0.5,
                    shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20.0)
            ),
                    child: Container(
                      // height: MediaQuery.of(context).size.height/6.5,
                      child: ListTile(
                        title: Text("Active Cameras",style: TextStyle(fontWeight: FontWeight.bold,)),
                        trailing: Padding(
                            padding: const EdgeInsets.only(right:30.0),
                           child: CircleAvatar(
                             backgroundColor: AppColors.colorGrey,
                             radius: 30.0,
                             child: Padding(
                               padding: const EdgeInsets.only(left:10.0,right: 10.0,bottom: 18.0),
                               child: TextFormField(
                                 textAlign: TextAlign.center,
                                 initialValue: "0",
                                 
                                 validator: (val){
          if(val.isEmpty){
            return "Required";
          }else{
            return null;
          }
        },
         onSaved: (val)=>_activeC=val,
                               ),
                             ),
                           ),
                          ),
                      ),
                
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left:18.0,right: 18.0),
                  child: Card(
                    // color: Colors.green,
                    elevation: 0.5,
                    shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20.0)
            ),
                    child: Container(
                      // height: MediaQuery.of(context).size.height/6.5,
                      child: ListTile(
                        title: Text("Total Cameras",style: TextStyle(fontWeight: FontWeight.bold,)),
                        trailing: Padding(
                            padding: const EdgeInsets.only(right:30.0),
                           child: CircleAvatar(
                             backgroundColor: AppColors.colorGrey,
                             radius: 30.0,
                             child: Padding(
                               padding: const EdgeInsets.only(left:10.0,right: 10.0,bottom: 18.0),
                               child: TextFormField(
                                 textAlign: TextAlign.center,
                                 initialValue: widget.details["total_camera"].toString(),
                                 validator: (val){
          if(val.isEmpty){
            return "Required";
          }else{
            return null;
          }
        },
         onSaved: (val)=>_totalC=val,
                               ),
                             ),
                           ),
                          ),
                      ),
                
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left:18.0,right: 18.0),
                  child: Card(
                    // color: Colors.green,
                    elevation: 0.5,
                    shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20.0)
            ),
                    child: Container(
                      // height: MediaQuery.of(context).size.height/6.5,
                      child: ListTile(
                        title: Text("Outer Cameras",style: TextStyle(fontWeight: FontWeight.bold,)),
                        trailing: Padding(
                            padding: const EdgeInsets.only(right:30.0),
                           child: CircleAvatar(
                             backgroundColor: AppColors.colorGrey,
                             radius: 30.0,
                             child: Padding(
                               padding: const EdgeInsets.only(left:10.0,right: 10.0,bottom: 18.0),
                               child: TextFormField(
                                 textAlign: TextAlign.center,
                                 initialValue: widget.details["outer_camera_channel_no"].toString(),
                                 validator: (val){
          if(val.isEmpty){
            return "Required";
          }else{
            return null;
          }
        },
         onSaved: (val)=>_outerC=val,
                               ),
                             ),
                           ),
                          ),
                      ),
                
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left:18.0,right: 18.0),
                  child: Card(
                    // color: Colors.green,
                    elevation: 0.5,
                    shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20.0)
            ),
                    child: Container(
                      // height: MediaQuery.of(context).size.height/6.5,
                      child: ListTile(
                        title: Text("Billing Cameras",style: TextStyle(fontWeight: FontWeight.bold,)),
                        trailing: Padding(
                            padding: const EdgeInsets.only(right:30.0),
                           child: CircleAvatar(
                             backgroundColor: AppColors.colorGrey,
                             radius: 30.0,
                             child: Padding(
                               padding: const EdgeInsets.only(left:10.0,right: 10.0,bottom: 18.0),
                               child: TextFormField(
                                 textAlign: TextAlign.center,
                                  initialValue: widget.details["billing_camera_channel_no"].toString(),
                                 validator: (val){
          if(val.isEmpty){
            return "Required";
          }else{
            return null;
          }
        },
         onSaved: (val)=>_billingC=val,
                               ),
                             ),
                           ),
                          ),
                      ),
                
                    ),
                  ),
                ),
              
                
                
                Padding(
                  padding: const EdgeInsets.all(18.0),
                  child: MyRaisedButton(
                    title: "Update",
                    loading: loading,
                    buttonColor: AppColors.colorBlue,
                    textColor: AppColors.colorWhite,
                    onPressed:updateData,
                  ),
                )

              ],
            ),
        ),
      )


          

    );
  }
}